#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw, ImageFont
import bs4
import argparse
import abc
import textwrap
import logging

logging.basicConfig(format='[%(asctime)s] - %(levelname)s - %(message)s',
                    datefmt='%a %b %d %Y %I:%M:%S%p')
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


class BasicHTMLParser(object):

    """Abstract base class for html parsers."""
    __metaclass__ = abc.ABCMeta

    def __init__(self, input_html, filecharset='utf-8'):
        self._wrap_width = 65
        self._tab_size = 4
        self._html_soup = self._parse_input(input_html, filecharset)
        self._html_parsed = self.parse_strings()

    def __str__(self):
        return unicode(self._html_soup)

    def __iter__(self):
        return iter(unicode(self._html_parsed).splitlines())

    def __len__(self):
        return len(self._html_parsed.splitlines())

    @property
    def html_parsed(self):
        return self._html_parsed

    @property
    def page_title(self):
        return self._html_soup.title.string

    @abc.abstractmethod
    def parse_strings(self):
        """Abstract method which carries out the process on the specified file.
        Returns True if successful, False otherwise."""
        pass

    @staticmethod
    def _parse_input(html, charset):
        """Had to explicit provide the utf-8 encoding"""
        if hasattr(html, 'read'):
            return bs4.BeautifulSoup(html.read().decode(charset), "html5lib")
        else:
            return bs4.BeautifulSoup(html.decode(charset), "html5lib")

    def _raw_string(self, string, tab=False, bulleted_tab=False):
        if not string:
            return u''
        prefix = u''
        tab_emul = u' ' * self._tab_size
        if bulleted_tab:
            prefix = unicode(tab_emul + u'\u2022 ')
        elif tab:
            prefix = unicode(tab_emul + u'  ')
        return unicode(textwrap.fill(prefix+string.strip().replace('\n', ' '),
                                     self._wrap_width))


class FirstHTMLParser(BasicHTMLParser):

    def __init__(self, input_html, filecharset='utf-8'):
        BasicHTMLParser.__init__(self, input_html, filecharset)

    def parse_strings(self):
        def recursion(parent, parsed):
            for top_elem in parent.contents:
                # TODO: the search for <ul> should be recursive
                if isinstance(top_elem, bs4.Tag) and top_elem.name == u'ul':
                    parsed.append(u'')  # add whiteline before list
                    for ul_elem in top_elem.contents:
                        # strips trailing '\n's and replaces inline '\n's
                        # with whitespace
                        if ul_elem.name == u'li':
                            # outputs <li>s text with tab and bullet
                            parsed.append(self._raw_string(ul_elem.string,
                                                           bulleted_tab=True))
                        elif ul_elem.string.strip():
                            # outputs any other text inside <ul>
                            parsed.append(self._raw_string(ul_elem.string,
                                                           tab=True))
                    # add whiteline after list
                    parsed.append(u'')
                elif isinstance(top_elem, bs4.Tag) and top_elem.name == u'br':
                    # add whitline as of <br>
                    parsed.append(u'')
                elif isinstance(top_elem, bs4.Tag):
                    recursion(top_elem, parsed)
                    # parsed.append(raw_string(top_elem.get_text()))
                else:
                    # any other tag or <body>'s text
                    parsed.append(self._raw_string(top_elem.string))
            return parsed

        parsed_lines = list()
        parsed_lines.append(self._html_soup.title.string + '\n\n')
        parsed_lines = recursion(self._html_soup.body, parsed_lines)

        LOGGER.debug("parsed lines: %s", (len(parsed_lines)))

        return '\n'.join(parsed_lines)


def draw_img_from_html(parser, image_name=None, text_color='black',
                       back_color='white', text_size=30,
                       text_font='OpenSans-Regular.ttf', startx=30, starty=30,
                       page_width=1200):
    image_name = (
        image_name or
        parser.page_title.lower().replace(' ', '_') + '.jpg' or
        'pic.jpg')
    page_size = page_width, (len(parser) + 2) * text_size
    image = Image.new('RGB', page_size, back_color)
    drawer = ImageDraw.Draw(image)
    font = ImageFont.truetype(text_font, text_size)

    for lin_num, line in enumerate(parser):
        drawer.text((startx, starty + lin_num * text_size),
                    unicode(line),
                    font=font,
                    fill=text_color)
    image.save(image_name)


def _setup_args():
    parser = argparse.ArgumentParser(prog=__file__)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--input-file', '-i', help='File to parse')
    group.add_argument('--input-string', '-s', help='String to parse')
    parser.add_argument('--image-name', '-n', help='Output image name')

    args = parser.parse_args()

    return args.input_file, args.input_string, args.image_name


def main():
    html_file, html_string, image_name = _setup_args()

    if html_file:
        html_input = open(html_file, 'r')
    else:
        html_input = html_string

    parsed_html = FirstHTMLParser(html_input)
    draw_img_from_html(parsed_html, image_name=image_name)

if __name__ == '__main__':
    main()

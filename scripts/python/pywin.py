# -*- coding: utf-8 -*-
"""Gathers info about files and folder in provided folder"""

import os
import csv
import struct

# TODO:
# 0. Divide in several csv files:
# - Files (name, size)
# - Folders (name, files_num)
# - bin x32 (name, size)
# - bin x64 (name, size)
# - bon other (name, size)
# - here will be 3 files:
# -- bin size
# -- bin count
# -- bin %
# 1. Divide files by bitness
# 2. Make this table:
# Table of statistic information
# N   Position    Size    Count   % of total file size
# 1   dll files   64.624 Mb   137 17.2094777107
# 2   exe files   77.759 Mb   79  20.5634157184
# 3   dll_32_bit  48.976 Mb   104 12.8266813593
# 4   dll_64_bit  16.672 Mb   33  4.38279635138
# 5   exe_32_bit  39.759 Mb   64  10.5765256624
# 6   exe_64_bit  37.0 Mb 15  9.98689005603
# 7   dll_unknown_bit 19.816 Mb   19  5.26347712342
# 8   exe_unknown_bit 24.302 Mb   11  6.48628928543
# 9   all_files_at_folder 374.896 Mb  13700   -
# Finished at 2015-05-18/09:21:27

COUNT = dict(files=0, folders=0, exe_x32=0, exe_x64=0,
             dll_x32=0, dll_x64=0, exe_other=0, dll_other=0)
SIZES = dict(files=0, exe_x32=0, exe_x64=0, dll_x32=0,
             dll_x64=0, exe_other=0, dll_other=0)


def get_the_bitness_of(f='c:\\windows\\explorer.exe'):
    # http://reverseengineering.stackexchange.com/questions/6040/check-if-exe-is-64-bit
    # 0x0000 - Assumed to be applicable to any machine type
    # 0x01d3 - Matsushita AM33
    # 0x8664 - x64
    # 0x01c0 - ARM little endian
    # 0x01c4 - ARMv7 (or higher) Thumb mode only
    # 0xaa64 - ARMv8 in 64-bit mode
    # 0x0ebc - EFI byte code
    # 0x014c - Intel 386 or later processors and compatible processors
    # 0x0200 - Intel Itanium processor family
    # 0x9041 - Mitsubishi M32R little endian
    # 0x0266 - MIPS16
    # 0x0366 - MIPS with FPU
    # 0x0466 - MIPS16 with FPU
    # 0x01f0 - Power PC little endian
    # 0x01f1 - Power PC with floating point support
    # 0x0166 - MIPS little endian
    # 0x01a2 - Hitachi SH3
    # 0x01a3 - Hitachi SH3 DSP
    # 0x01a6 - Hitachi SH4
    # 0x01a8 - Hitachi SH5
    # 0x01c2 - ARM or Thumb (“interworking”)
    # 0x0169 - MIPS little-endian WCE v2

    IMAGE_FILE_MACHINE = {0x454e: "x16",
                          0x014c: "x32",
                          0x010b: "x32",
                          0x0200: "x64",
                          0x8664: "x64",
                          0xaa64: "x64",
                          0x01c4: "armv7"}

    with open(f, "rb", buffering=0) as fh:
        if fh.read(2) != "MZ":
            machine = ""
        else:
            fh.seek(60)
            header_offset = struct.unpack("L", fh.read(4))[0]
            fh.seek(header_offset + 4)
            machine = struct.unpack("H", fh.read(2))[0]

    return IMAGE_FILE_MACHINE.get(machine, "")


def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def enumerate_files(folder='c:\\ecloud'):
    for base, folders, files in os.walk(folder):
        COUNT['files'] += len(files)
        COUNT['folders'] += len(folders)
        for filename in files:
            fp = os.path.join(base, filename)
            fe = os.path.splitext(filename.lower())[1]
            fs = os.path.getsize(fp)
            fa = ""
            if fe in ['.exe', '.dll']:
                fa = get_the_bitness_of(fp)
                if fa in ['x32', 'x64']:
                    key = fe[1:] + '_' + fa
                else:
                    key = fe[1:] + '_other'
                COUNT[key] += 1
                SIZES[key] += fs
            SIZES['files'] += fs

            yield dict(file_path=fp, file_size=sizeof_fmt(fs), file_arch=fa)


def main():
    with open('report_1.csv', 'wb', buffering=1) as csvfile:
        fieldnames = ['file_path', 'file_size', 'file_arch']
        csvwriter = csv.DictWriter(
            csvfile, fieldnames=fieldnames, quoting=csv.QUOTE_ALL)
        csvwriter.writeheader()
        csvwriter.writerows(enumerate_files())
    with open('report_2.csv', 'wb') as csvfile:
        csvwriter = csv.DictWriter(
            csvfile, fieldnames=COUNT.keys(), quoting=csv.QUOTE_ALL)
        csvwriter.writeheader()
        csvwriter.writerow(COUNT)
    with open('report_3.csv', 'wb') as csvfile:
        csvwriter = csv.DictWriter(
            csvfile, fieldnames=SIZES.keys(), quoting=csv.QUOTE_ALL)
        csvwriter.writeheader()
        SIZES.update((k, sizeof_fmt(v)) for k, v in SIZES.items())
        csvwriter.writerow(SIZES)


if __name__ == '__main__':
    main()

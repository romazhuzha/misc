# -*- coding: utf-8 -*-
"""Provides simple wrapper for HipChat REST API"""

import urlparse
import requests
import logging
import pprint
import yaml
import os

PP = pprint.PrettyPrinter(width=20)
logging.basicConfig(format='[%(asctime)s] - %(levelname)s - %(message)s')
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

HOST = 'https://api.hipchat.com/v2/'

os.chdir(os.path.dirname(os.path.abspath(__file__)))
USER_CONFIG = yaml.load(open('hipchat.yml', 'r'))

AUTH_TOKEN = USER_CONFIG['credentials']['hipchat_token']
ROOM_ID = USER_CONFIG['default']['hipchat_default_room_id']

print USER_CONFIG, AUTH_TOKEN, ROOM_ID


def URL(url):
    return urlparse.urljoin(HOST, url)

"""
Functions to implement:
get_all_rooms          GET  /v2/room?max-results=1000
get_room               GET  /v2/room/{room_id_or_name}
send_room_notification POST /v2/room/{id_or_name}/notification
    {"color": "yellow, green, red, purple, gray, random",
     "message": "",
     "notify": "true, false",
     "message_format": "html, text"}

Future:
get_all_users          GET  /v2/user?max-results=1000
send_private_message   POST /v2/user/{id_or_email}/message
    {"message": "",
     "notify": "true, false",
     "message_format": "html, text"}
"""


def get_all_rooms():
    url = URL('room')
    params = {'max-results': '1000', 'auth_token': AUTH_TOKEN}
    req = requests.get(url, params=params)
    req_json = req.json()
    return {room_item['id']: room_item['name'] for room_item in req_json['items']}


def get_room_id_by_name(room_name):
    room_id = -1
    for key, value in get_all_rooms().items():
        if value == room_name:
            room_id = key
    return room_id


def get_room_by_name(room_name):
    return get_room_by_id(get_room_id_by_name(room_name))


def get_room_by_id(room_id=ROOM_ID):
    url = URL(urlparse.urljoin('room/', str(room_id)))
    params = {'auth_token': AUTH_TOKEN}
    req = requests.get(url, params=params)
    return req.json()


def generate_new_token():
    logging.debug('get_all_rooms')
    url = URL(urlparse.urljoin('oauth/', 'token'))
    params = {'auth_token': AUTH_TOKEN}
    data = {'grant_type': 'personal'}
    req = requests.post(url, params=params, json=data)
    return req.json()


def send_room_notification(msg, room_id=ROOM_ID, msg_format='html', color='purple', notify=True):
    logging.debug('get_all_rooms')
    url = URL('room/' + str(room_id) + '/notification')
    LOGGER.debug(url)
    params = {'auth_token': AUTH_TOKEN}
    LOGGER.debug(params)
    data = {'message': msg, 'message_format': msg_format,
            'color': color, 'notify': notify}
    LOGGER.debug(data)
    req = requests.post(url, params=params, json=data)
    if req.status_code != 204:
        LOGGER.error(req.json())

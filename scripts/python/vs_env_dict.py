import os
import subprocess as sub


def vs_env_dict(vsver):
    cmd = r'cmd /s /c ""%VS{0}COMNTOOLS%vsvars32.bat" & set"'.format(vsver)
    output = sub.Popen(cmd, stdout=sub.PIPE, stderr=sub.STDOUT, stdin=sub.PIPE).communicate()[0].split("\r\n")

    return dict((e[0].upper(), e[1]) for e in [p.rstrip().split("=", 1) for p in output] if len(e) == 2)

os.environ.update(vs_env_dict(100))
sub.call('devenv /?', shell=True)
